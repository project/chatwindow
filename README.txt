## Starting action server



python3.6 -m rasa_core_sdk.endpoint --actions actions



## Starting rasa server 



python3.6 run_app.py




## Configuration



In generalconfig.py change to your Drupal installation path and opencv path




## Training



## Training the NLU model



python3.6 nlu_model.py



## Train the Rasa Core model



python3.6 dialogue_management_model.py



## Interactive training session


python3.6 train_interactive.py





